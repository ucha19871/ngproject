var app = angular.module('UchApp', []);


app.controller('StoreController', function ($scope) {
    this.pr = gems;

});

app.controller('PanelController', function () {
    this.tab = 1;
    this.selectTab = function(setTab){ this.tab = setTab; };
    this.isSelected = function(checkTab){ return this.tab === checkTab };


});

app.controller('ReviewController',function(){
    this.review = {};

    this.addReview = function(product){
        product.reviews.push(this.review);
        this.review = {};
    };

});


var gems = [
    {
        name: 'GuduTentoni',
        price: 231.8,
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.',
        canPurchase: true,
        soldOut: true,
        image: [
            {
                full: 'http://placehold.it/800x600.png',
                thumb: 'http://placehold.it/150x100.png'
            }
        ],
        reviews : [
            {
                stars: 8,
                body: "I love this product",
                author: 'ucha19871@gmail.com'
            },
            {
                stars: 9,
                body: "I love this product most",
                author: 'd.ylif@yahoo.com'
            }
        ]
    },
    {
        name: 'Rododendroni',
        price: 25.8,
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.',
        canPurchase: true,
        soldOut: true,
        image: [
            {
                full: 'http://placehold.it/800x600.png',
                thumb: 'http://placehold.it/150x100.png'
            }
        ],
        reviews : [
            {
                stars: 7,
                body: "thats nice product",
                author: 't.asd@gmail.com'
            }
        ]
    },
    {
        name: 'Armagedoni',
        price: 30.5,
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.',
        canPurchase: false,
        soldOut: true,
        image: [
            {
                full: 'http://placehold.it/800x600.png',
                thumb: 'http://placehold.it/150x100.png'
            }
        ],
        reviews : [
            {
                stars: 1,
                body: "That's a one shitty product right there",
                author: 'klitori@yahoo.com'
            }
        ]
    },
    {
        name: 'Tetraxendroni',
        price: 25.8,
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.',
        canPurchase: true,
        soldOut: false,
        image: [
            {
                full: 'http://placehold.it/800x600.png',
                thumb: 'http://placehold.it/150x100.png'
            }
        ],
        reviews : [
            {
                stars: 0,
                body: "",
                author: ''
            }
        ]
    }
];













